import {
    Badge as BadgeComponent,
    BadgeProps as BadgeComponentProps,
    Skeleton
} from "@mantine/core";

interface BadgeProps extends BadgeComponentProps<'div'> {
    isLoading?: boolean;
}

const Badge = ({isLoading, variant = "dot", ...props}: BadgeProps) =>
    <Skeleton visible={isLoading} width="" radius="xl">
        <BadgeComponent variant={variant} {...props} />
    </Skeleton>

export default Badge