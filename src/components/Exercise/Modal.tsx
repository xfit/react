import {Modal as ModalComponent, Skeleton, Title} from '@mantine/core'
import {useNavigate, useParams} from "react-router-dom";
import {useState} from "react";
import {Details as ExerciseDetailsType} from "types/Exercise"
import {Details as ExerciseDetails} from "components/Exercise";
import {useQuery} from "react-query";
import {getExerciseByName} from "api/exerciseService";

const Modal = () => {
    const [opened, setOpened] = useState(true);

    const navigate = useNavigate();
    const {name} = useParams<'name'>();

    const onDismiss = () => {
        setOpened(false);
        navigate(-1);
    };

    const {data: exercise, isFetching, isLoading } = useQuery<ExerciseDetailsType, Error>(
        ['exercise', name],
        () => getExerciseByName(name || "")
    );

    return (
        <ModalComponent
            opened={opened}
            onClose={onDismiss}
            title={
                <Skeleton visible={isFetching || isLoading} >
                    <Title order={3}>{exercise?.title}</Title>
                </Skeleton>
            }
            size="xl"
        >
            {exercise && <ExerciseDetails exercise={exercise} isLoading={isFetching || isLoading}/>}
        </ModalComponent>
    );
}

export default Modal