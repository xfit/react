import {Summary as ExerciseSummary} from "types/Exercise";
import {Card, Group, SimpleGrid, Skeleton, Text, Title} from "@mantine/core";
import React from "react";
import {Badge, TypeIcon} from "components/Exercise";
import {Link, useLocation} from "react-router-dom";

interface ListItemProps {
    exercise: ExerciseSummary;
    isLoading?: boolean;
}

export const ListItem = (
    {
        exercise: {type, title, primer, primary, secondary, equipment},
        isLoading
    }: ListItemProps
) =>
    <Card shadow="md" radius="md">
        <Group spacing={4}>
            <Skeleton visible={isLoading} width="" radius="xl">
                <TypeIcon type={type}/>
            </Skeleton>

            <Skeleton visible={isLoading} width="">
                <Title order={4}>{title}</Title>
            </Skeleton>
        </Group>

        <Group my="xs" spacing="xs">
            {primary.map((muscle, index) =>
                <Badge key={index} isLoading={isLoading} color="red">{muscle}</Badge>)}
            {secondary.map((muscle, index) =>
                <Badge key={index} isLoading={isLoading} color="orange">{muscle}</Badge>)}
            {equipment.map((equipment, index) =>
                <Badge key={index} isLoading={isLoading} color="blue">{equipment}</Badge>)}
        </Group>

        <Skeleton visible={isLoading}>
            <Text>{primer}</Text>
        </Skeleton>
    </Card>


interface ListProps {
    exercises: ExerciseSummary[];
    isLoading?: boolean;
}

const List = ({exercises = [], isLoading = false}: ListProps) => {
    const location = useLocation();

    return (
        <SimpleGrid my="md" cols={1}>
            {exercises.map((exercise) =>
                <Link
                    key={exercise.id}
                    to={`/exercise/${exercise.name}`}
                    state={{backgroundLocation: location}}
                    style={{textDecoration: "none"}}
                >
                    <ListItem key={exercise.id} exercise={exercise} isLoading={isLoading}/>
                </Link>
            )}
        </SimpleGrid>
    );
}

export default List