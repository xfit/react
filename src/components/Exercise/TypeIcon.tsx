import {ThemeIcon} from "@mantine/core";
import {FaBullseye, FaLayerGroup, FaPenSquare, FaQuestionCircle, FaStopwatch} from "react-icons/fa"
import {Type as ExerciseType} from "types/Exercise";

export const ICON = {
    "isometric": FaStopwatch,
    "compound": FaLayerGroup,
    "isolation": FaBullseye,
    "custom": FaPenSquare
}

interface TypeIconProps {
    type: ExerciseType;
}

const TypeIcon = ({type}: TypeIconProps) =>
    <ThemeIcon radius="lg" color="gray" variant="outline" style={{border: "none"}}>
        {(ICON[type] || FaQuestionCircle)({size: 16})}
    </ThemeIcon>

export default TypeIcon