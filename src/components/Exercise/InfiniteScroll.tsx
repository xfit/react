import ReactInfiniteScroll from "react-infinite-scroll-component";
import {useInfiniteQuery} from "react-query";
import {InfinitePage} from "types/api";
import {Summary as ExerciseSummary} from "types/Exercise";
import {getInfiniteExerciseSummaryPage} from "api/exerciseService";
import {List as ExerciseList} from 'components/Exercise';
import {useEffect, useState} from "react";


const InfiniteScroll = () => {
    const [pages, setPages] = useState<InfinitePage<ExerciseSummary>[]>([]);

    const {data, hasNextPage = false, fetchNextPage} = useInfiniteQuery<InfinitePage<ExerciseSummary>>(
        'exerciseSummaries',
        ({pageParam = 0}) => getInfiniteExerciseSummaryPage(pageParam),
        {getNextPageParam: (lastPage) => lastPage.next}
    );

    useEffect(() => {
        data && setPages(data.pages)
    }, [data]);

    const loader = pages.slice(-1).map(({content}, index) =>
        <ExerciseList key={index} exercises={content} isLoading/>);

    const items = pages.map(({content}, index) =>
        <ExerciseList key={index} exercises={content}/>);

    return (
        <ReactInfiniteScroll
            dataLength={items.length}
            next={fetchNextPage}
            hasMore={hasNextPage}
            loader={loader}
            children={items}
        />
    );
}

export default InfiniteScroll
