import {Details as ExerciseDetails} from "types/Exercise";
import {Divider, Group, Skeleton, Text, ThemeIcon, Title} from "@mantine/core";
import {FaCircle, FaDumbbell, FaFire, FaQuestionCircle} from "react-icons/fa";
import {ICON as TYPE_ICON} from "components/Exercise/TypeIcon";
import {Badge} from "components/Exercise/index";
import React from "react";
import {IconType} from "react-icons";

interface BadgeRowProps {
    icon: IconType;
    title: string;
    color: string;
    labels?: string[];
    isLoading?: boolean;
}

const BadgeRow = ({icon, title, color, labels, isLoading}: BadgeRowProps) =>
    <tr>
        <td>
            <Skeleton visible={isLoading} width="" radius="xl">
                <ThemeIcon radius="lg" color="gray" variant="outline" style={{border: "none"}}>
                    {icon({size: 16})}
                </ThemeIcon>
            </Skeleton>
        </td>
        <td>
            <Skeleton visible={isLoading}>
                <Text weight="bold" color="gray">{title}</Text>
            </Skeleton>
        </td>
        <td align="left">
            <Group spacing={3}>
                {labels?.map((label, index) =>
                    <Badge key={index} isLoading={isLoading} color={color}>{label}</Badge>)}
            </Group>
        </td>
    </tr>


interface StringListProps {
    title?: string;
    stringArr: string[];
    isLoading?: boolean;
}

const StringList = ({title, stringArr, isLoading}: StringListProps) =>
    <>
        {title &&
            <Skeleton visible={isLoading}>
                <Title order={3}>{title}</Title>
            </Skeleton>}

        <Skeleton visible={isLoading}>
            <ul>
                {stringArr.map((string, index) =>
                    <li key={index}>
                        <Text>{string}</Text>
                    </li>)}
            </ul>
        </Skeleton>
    </>


interface DetailsProps {
    exercise: ExerciseDetails;
    isLoading?: boolean;
}

export const Details = (
    {
        exercise: {type, primer, primary, secondary, equipment, steps, tips, references},
        isLoading
    }: DetailsProps) =>
    <>
        <table>
            <BadgeRow icon={TYPE_ICON[type] || FaQuestionCircle} title={"Type"}
                      color="green" isLoading={isLoading}
                      labels={[type]}/>

            {primary.length > 0 &&
                <BadgeRow icon={FaFire} title="Primary Muscle"
                          color="red" isLoading={isLoading}
                          labels={primary.map(({name}) => name)}/>}

            {secondary.length > 0 &&
                <BadgeRow icon={FaCircle} title="Secondary Muscle"
                          color="orange" isLoading={isLoading}
                          labels={secondary.map(({name}) => name)}/>}

            {equipment.length > 0 &&
                <BadgeRow icon={FaDumbbell} title="Equipment"
                          color="blue" isLoading={isLoading}
                          labels={equipment.map(({name}) => name)}/>}
        </table>

        <Divider my="md"/>

        <Skeleton visible={isLoading}>
            <Text size="lg">{primer}</Text>
        </Skeleton>

        {steps.length > 0 &&
            <StringList stringArr={steps} isLoading={isLoading}/>}

        {tips.length > 0 &&
            <StringList title="Tips" stringArr={tips} isLoading={isLoading}/>}

        {references.length > 0 &&
            <StringList title="References" stringArr={references} isLoading={isLoading}/>}
    </>

export default Details
