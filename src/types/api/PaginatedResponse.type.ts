export type Embedded<T> = {
    [key: string]: T[]
}

export type Link = {
    href: string;
}

export type Links = {
    [key: string]: Link;
}

export type PageDetails = {
    number: number;
    size: number;
    totalElements: number;
    totalPages: number;
}

type PaginatedResponse<T> = {
    _embedded: Embedded<T>;
    _links: Links;
    page: PageDetails;
}

export default PaginatedResponse
