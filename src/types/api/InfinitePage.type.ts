type InfinitePage<T> = {
    next: number | undefined;
    content: T[];
}

export default InfinitePage