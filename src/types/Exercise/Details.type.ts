import {Links as ApiLinks} from 'types/api';
import Properties from './Properties.type'

export type RelatedProperty = {
    id: number;
    name: string;
    _links?: ApiLinks;
};

export type Muscle = RelatedProperty & {};
export type Equipment = RelatedProperty & {};

type Details = Properties & {
    primary: Muscle[];
    secondary: Muscle[];
    equipment: Equipment[];
    steps: string[];
    tips: string[];
    references: string[];
};

export default Details;
