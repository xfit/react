import {Links as ApiLinks} from "types/api";

export type Type = "isometric" | "compound" | "isolation" | "custom";

type Properties = {
    id: number;
    name: string;
    title: string;
    primer: string;
    type: Type;
    _links: ApiLinks;
};

export default Properties;
