import Properties from "./Properties.type";

type Summary = Properties & {
    primary: string[];
    secondary: string[];
    equipment: string[];
};

export default Summary;
