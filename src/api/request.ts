import axios, {AxiosResponse, Method} from "axios";
import {InfinitePage, PaginatedResponse} from "types/api";

const api = axios.create({
    baseURL: "http://localhost:3000/api/v1",
});

const request = <T>(
    method: Method,
    url: string,
    params?: any
): Promise<AxiosResponse<T>> => api.request<T>({method, url, params});


export const get = <T>(url: string, params?: any) =>
    request<T>("GET", url, params);


export const getInfinitePage = <T>(
    url: string,
    page: number,
    relationName: string
): Promise<InfinitePage<T>> =>
    get<PaginatedResponse<T>>(url, {page})
        .then((response) => response.data)
        .then(
            ({page: {number, totalPages}, _embedded}) => ({
                next: number + 1 < totalPages ? number + 1 : undefined,
                content: _embedded[relationName]
            }))

export default request;
