import {get, getInfinitePage} from "api/request"
import {
    Summary as ExerciseSummary,
    Details as ExerciseDetails
} from 'types/Exercise'

const BASE_PATH = "exercise";

export const getInfiniteExerciseSummaryPage = (page: number) =>
    getInfinitePage<ExerciseSummary>(
        `${BASE_PATH}/summary`, page, 'exerciseSummaryCollection');

export const getExerciseByName = (name: string) =>
    get<ExerciseDetails>(`${BASE_PATH}/${name}`)
        .then(response => response.data);