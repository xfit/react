import {Container} from "@mantine/core";
import {InfiniteScroll as ExerciseInfiniteScroll} from "components/Exercise";


const ExercisesPage = () => {
    return (
        <Container>
            <ExerciseInfiniteScroll/>
        </Container>
    )
}

export default ExercisesPage