import {lazy} from "react"
import {Route, Routes, useLocation} from "react-router-dom"
import {QueryClient, QueryClientProvider} from "react-query";
import {Modal as ExerciseModal} from "./components/Exercise"

const ExercisesPage = lazy(() => import('pages/ExercisesPage'))

const queryClient = new QueryClient()

const App = () => {
    let location = useLocation();

    let state = location.state as { backgroundLocation?: Location };

    return (
        <QueryClientProvider client={queryClient}>
            <Routes location={state?.backgroundLocation || location}>
                <Route path="exercise" element={<ExercisesPage/>}>
                    <Route path=":name" element={<p>test</p>}/>
                </Route>
            </Routes>
            {state?.backgroundLocation &&
                (<Routes>
                    <Route path="exercise/:name" element={<ExerciseModal/>}/>
                </Routes>)}
        </QueryClientProvider>
    );
}

export default App